<?php

/*
 * This file is part of the Sonata project.
 *
 * (c) Thomas Rabaix <thomas.rabaix@sonata-project.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * ExperienciaHasMedia
 * @ORM\Table()
 * @ORM\Entity
 */
class ExperienciaHasMedia
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \Application\Sonata\MediaBundle\Entity\Media
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"}, fetch="LAZY")
     * @ORM\JoinColumn(name="media_id", referencedColumnName="id")
     */
    protected $media;



    /**
     * @var \AppBundle\Entity\Experiencia
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Experiencia", cascade={"persist","remove"}, inversedBy="images", fetch="LAZY")
     * @ORM\JoinColumn(name="experiencia_id", referencedColumnName="id",nullable=true)
     */
    protected $experiencia;

    /**
     * @var integer
     * @ORM\Column(name="position", type="integer")
     */
    protected $position;

    /**
     * @var \DateTime
     * @ORM\Column(name="updated", type="datetime")
     */
    protected $updated;
    /**
     * @var \DateTime
     * @ORM\Column(name="created", type="datetime")
     */
    protected $created;

    /**
     * @var boolean
     * @ORM\Column(name="enable", type="boolean")
     */
    protected $enabled;


    /**
     * @var boolean
     * @ORM\Column(name="delete_footer", type="boolean")
     */
    protected $deleteFooter;

    public function __construct()
    {
        $this->position = 0;
        $this->enabled  = false;
        $this->deleteFooter  = false;
        $this->updated = new \DateTime('now');
        $this->created = new \DateTime('now');
    }
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * {@inheritdoc}
     */
    public function setCreated(\DateTime $created = null)
    {
        $this->created = $created;
    }

    /**
     * {@inheritdoc}
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * {@inheritdoc}
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    }

    /**
     * {@inheritdoc}
     */
    public function getEnabled()
    {
        return $this->enabled;
    }
    /**
     * {@inheritdoc}
     */
    public function setDeleteFooter($deleteFooter)
    {
        $this->deleteFooter = $deleteFooter;
    }

    /**
     * {@inheritdoc}
     */
    public function getDeleteFooter()
    {
        return $this->deleteFooter;
    }

    /**
     * {@inheritdoc}
     */
    public function setExperiencia(\AppBundle\Entity\Experiencia $experiencia = null)
    {
        $this->experiencia = $experiencia;
    }

    /**
     * {@inheritdoc}
     */
    public function getExperiencia()
    {
        return $this->experiencia;
    }

    /**
     * {@inheritdoc}
     */
    public function setMedia(\Sonata\MediaBundle\Model\MediaInterface $media = null)
    {
        $this->media = $media;
    }

    /**
     * {@inheritdoc}
     */
    public function getMedia()
    {
        return $this->media;
    }


    /**
     * {@inheritdoc}
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * {@inheritdoc}
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * {@inheritdoc}
     */
    public function setUpdated(\DateTime $updated = null)
    {
        $this->updated = $updated;
    }

    /**
     * {@inheritdoc}
     */
    public function getUpdated()
    {
        return $this->updated;
    }



    /**
     * {@inheritdoc}
     */
    public function __toString()
    {
        return $this->getExperiencia().' | '.$this->getMedia();
    }

}
