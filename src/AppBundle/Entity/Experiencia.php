<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Constraints as Assert;
use Oh\GoogleMapFormTypeBundle\Validator\Constraints as OhAssert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Experiencia
 *
 * @ORM\Table(name="experiencia")
 * @ORM\Entity
 */
class Experiencia
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=255)
     */
    private $titulo;

    /**
     * Muchas experiencias a un usuario.
     * @ORM\ManyToOne(targetEntity="\Application\Sonata\UserBundle\Entity\User", inversedBy="experiencias")
     * @ORM\JoinColumn(name="host_id", referencedColumnName="id")
     */
    private $host;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="text")
     */
    private $descripcion;

    /**
     * @var int
     *
     * @ORM\Column(name="experiencia", type="integer")
     */
    private $experiencia;

    /**
     * @var int
     *
     * @ORM\Column(name="personas", type="integer")
     */
    private $personas;

    /**
     * @var int
     *
     * @ORM\Column(name="horas", type="integer")
     */
    private $horas;

    /**
     * @var int
     *
     * @ORM\Column(name="valor", type="integer")
     */
    private $valor;

    /**
     * @var string
     *
     * @ORM\Column(name="requisitos", type="text")
     */
    private $requisitos;

    /**
     * @var string
     *
     * @ORM\Column(name="incluido", type="text")
     */
    private $incluido;

    /**
     * @var array
     *
     * @ORM\Column(name="place", type="array")
     */
    private $place;

    /**
      * @ORM\OneToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist","remove"})
      */
    private $picture;

    /**
     * @Assert\NotBlank()
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\ExperienciaHasMedia", mappedBy="experiencia" ,cascade={"persist","remove"} )
     */
    protected $images;

    /**
     * @ORM\ManyToOne(targetEntity="Categoria", inversedBy="experiencia")
     * @ORM\JoinColumn(name="categoria_id", referencedColumnName="id",nullable=true)
     */
    private $categoria;


    /**
     * @var string
     *
     * @ORM\Column(name="pago", type="string", length=255)
     */
     private $pago;


    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=255)
     */
     private $status;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     *
     * @return Experiencia
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Experiencia
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set experiencia
     *
     * @param integer $experiencia
     *
     * @return Experiencia
     */
    public function setExperiencia($experiencia)
    {
        $this->experiencia = $experiencia;

        return $this;
    }

    /**
     * Get experiencia
     *
     * @return int
     */
    public function getExperiencia()
    {
        return $this->experiencia;
    }

    /**
     * Set personas
     *
     * @param integer $personas
     *
     * @return Experiencia
     */
    public function setPersonas($personas)
    {
        $this->personas = $personas;

        return $this;
    }

    /**
     * Get personas
     *
     * @return int
     */
    public function getPersonas()
    {
        return $this->personas;
    }

    /**
     * Set horas
     *
     * @param integer $horas
     *
     * @return Experiencia
     */
    public function setHoras($horas)
    {
        $this->horas = $horas;

        return $this;
    }

    /**
     * Get horas
     *
     * @return int
     */
    public function getHoras()
    {
        return $this->horas;
    }

    /**
     * Set valor
     *
     * @param integer $valor
     *
     * @return Experiencia
     */
    public function setValor($valor)
    {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor
     *
     * @return int
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Set requisitos
     *
     * @param string $requisitos
     *
     * @return Experiencia
     */
    public function setRequisitos($requisitos)
    {
        $this->requisitos = $requisitos;

        return $this;
    }

    /**
     * Get requisitos
     *
     * @return string
     */
    public function getRequisitos()
    {
        return $this->requisitos;
    }

    /**
     * Set place
     *
     * @param array $place
     *
     * @return Experiencia
     */
    public function setPlace($place)
    {
        $this->place = $place;

        return $this;
    }

    /**
     * Get place
     *
     * @return array
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * Set categoria
     *
     * @param \AppBundle\Entity\Categoria $categoria
     *
     * @return Experiencia
     */
    public function setCategoria(\AppBundle\Entity\Categoria $categoria = null)
    {
        $this->categoria = $categoria;

        return $this;
    }

    /**
     * Get categoria
     *
     * @return \AppBundle\Entity\Categoria
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * Set pago
     *
     * @param string $pago
     *
     * @return Experiencia
     */
    public function setPago($pago)
    {
        $this->pago = $pago;

        return $this;
    }

    /**
     * Get pago
     *
     * @return string
     */
    public function getPago()
    {
        return $this->pago;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Experiencia
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }


    /**
     * Set incluido
     *
     * @param string $incluido
     *
     * @return Experiencia
     */
    public function setIncluido($incluido)
    {
        $this->incluido = $incluido;

        return $this;
    }

    /**
     * Get incluido
     *
     * @return string
     */
    public function getIncluido()
    {
        return $this->incluido;
    }

    /**
     * Set host
     *
     * @param \Application\Sonata\UserBundle\Entity\User $host
     * @return Experiencia
     */
    public function setHost(\Application\Sonata\UserBundle\Entity\User $host = null)
    {
        $this->host = $host;

        return $this;
    }

    /**
     * Get host
     *
     * @return \Application\Sonata\UserBundle\Entity\User
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * Set picture
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media $picture
     * @return Experiencia
     */
    public function setPicture(\Application\Sonata\MediaBundle\Entity\Media $picture = null)
    {
        $this->picture = $picture;

        return $this;
    }

    /**
     * Get picture
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
    }


    /**
     * Remove images
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media $images
     */
    public function removeImages(\AppBundle\Entity\ExperienciaHasMedia $images)
    {
        $this->images->removeElement($images);
    }
    /**
     * Get images
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Add images
     *
     * @param \AppBundle\Entity\ExperienciaHasMedia $images
     * @return Experiencia
     */
    public function addImages(\AppBundle\Entity\ExperienciaHasMedia $images)
    {
        $images->setExperiencia($this);
        $this->images[] = $images;

        return $this;
    }

    public function setImages($images)
      {
          $this->images = new ArrayCollection();

          foreach ($images as $experiencia) {
              $this->addImages($experiencia);
          }
      }


}
