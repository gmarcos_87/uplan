<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Categoria
 *
 * @ORM\Table(name="categoria")
 * @ORM\Entity
 */
class Categoria
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="color", type="string", length=10)
     */
    private $color;

    /**
     * @ORM\OneToMany(targetEntity="Experiencia", mappedBy="categoria")
     */
    private $experiencias;

    public function __construct()
    {
        $this->experiencias = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Categoria
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set color
     *
     * @param string $color
     *
     * @return Categoria
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Add experiencia
     *
     * @param \AppBundle\Entity\Experiencia $experiencia
     *
     * @return Categoria
     */
    public function addExperiencia(\AppBundle\Entity\Experiencia $experiencia)
    {
        $this->experiencias[] = $experiencia;

        return $this;
    }

    /**
     * Remove experiencia
     *
     * @param \AppBundle\Entity\Experiencia $experiencia
     */
    public function removeExperiencia(\AppBundle\Entity\Experiencia $experiencia)
    {
        $this->experiencias->removeElement($experiencia);
    }

    /**
     * Get experiencias
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getExperiencias()
    {
        return $this->experiencias;
    }

    public function __toString()
    {
    return $this->getNombre();
    }
}
