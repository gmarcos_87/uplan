<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;


class ExperienciaAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Contenido',array('class' => 'col-md-9'))
              ->add('titulo', 'text')
              ->add('descripcion', 'textarea')
              ->add('requisitos','textarea')
              ->add('incluido','textarea')
              ->add('place','oh_google_maps',array(
                  'label'          => 'Ubicación',
                  'type'           => 'hidden',  // the types to render the lat and lng fields as
                  'options'        => array(), // the options for both the fields
                  'lat_options'    => array(),   // the options for just the lat field
                  'lng_options'    => array(),    // the options for just the lng field
                  'lat_name'       => 'lat',   // the name of the lat field
                  'lng_name'       => 'lng',   // the name of the lng field
                  'map_width'      => '100%',     // the width of the map
                  'map_height'     => '300px',     // the height of the map
                  'default_lat'    => -34.5779392,    // the starting position on the map
                  'default_lng'    => -58.4111533, // the starting position on the map
                  'include_jquery' => false,   // jquery needs to be included above the field (ie not at the bottom of the page)
                  'include_gmaps_js'=>true,     // is this the best place to include the google maps javascript?
              ))
              ->add('picture','sonata_media_type',array(
                'label'=>'Imágne destacada',
                'context'=>'default',
                'provider'=>'sonata.media.provider.image'
              ))
              ->add('images', 'sonata_type_collection', array(
                      'cascade_validation' => false,
                      'label'=>'Imágenes secundarias',
                       'by_reference' => false,
                      'type_options' => array('delete' => false),
                  ), array(
                      'edit' => 'inline',
                      'inline' => 'table',
                      'sortable' => 'position',
                      'image_parameters' => array(
                        'context' => 'default',
                        'provider' => 'sonata.media.provider.image'
                      ),
                      'admin_code'=>'sonata.admin.experiencia_has_media'
                   )
              )
            ->end()
            ->with('Parámetros',array('class' => 'col-md-3'))
                ->add('host', 'entity', array(
                    'class' => 'Application\Sonata\UserBundle\Entity\User',
                    'choice_label' => 'username',))
                ->add('categoria', 'entity', array(
                    'class' => 'AppBundle\Entity\Categoria'))
                ->add('experiencia','integer', array(
                        'label'=>'Experiencia como anfitrión',
                        'attr' => array('min' => 0, 'max' => 5)
                ))
                ->add('personas','integer',array('label'=>'Personas simultaneas'))
                ->add('horas','integer',array('label'=>'Horas requeridas'))
                ->add('valor','money',array('label'=>'Valor', 'currency'=>'USD'))
                ->add('pago','choice',array(
                    'multiple'=> false,
                    'choices' => array(
                        'Mercadopago'=>'MERCADOPAGO',
                        'Efectivo'=>'CASH',
                        'Transferencia bancaria'=>'BANK'
                    )
                ))
                ->add('status','choice',array(
                    'multiple'=> false,
                    'choices' => array(
                        'Pendiente'=>'PENDING',
                        'Publicada'=>'PUBLISHED' )
                ))
            ->end()
            ;


    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('titulo');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('titulo')
            ->add('categoria.nombre','text',array('label'=>'Categoria'))
            ->add('picture', 'string', array('label'=>'Imágen','template'=>'SonataMediaBundle:MediaAdmin:list_image.html.twig'))
            ->add('valor', 'money',array('currency'=>'USD'))
            ->add('pago', 'text',array('label'=>'Cobro'))
            ->add('status','text',array('label'=>'Estado'))
            ->add('_action', 'actions', array(
                'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
            ;
    }
}
