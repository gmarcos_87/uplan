# Uplan

Código fuente de Uplan en Symfony2, para instalarlo recomiendo usar los contenedores docker de este link
(https://github.com/gmarcos87/other-docke-symfony)

## Requisitos para desarrollar
* Symfony2
* composer
* PHP7
* PHP7-GD
* MYSQL

## Pasos a seguir para desarrollar
Clonar este repositorio y configurar los parámetros de la base de datos en app/config/parameters.yml

- Instalación de paquetes
```
composer install
```
- Creación de la base de datos
```
php app/console doctrine:schema:create
```
- Instalación de assets
```
php app/console assets:install
```
- Generacón de asstics
```
php app/console assetic:dump
```
- Creación de usuario
```
php app/console fos:user:create admin admin@uplan.com abc123
```
- Asignación de rol de administrador
```
php app/console fos:user:promote admin --super
```
- Hacer correr el servidor
```
php app/console server:run
```

Con eso debería estar andando en el puerto 8000 y responder a la url http://localhost:8000/admin

## Docker-compose y deploy
En https://github.com/gmarcos87/other-docke-symfony van a encontrar una composición que permite hacer el deploy a producción de tres servidores, uno con php, otro con mysql y otro con nginx como proxy.
Myql y nginx tienen un funcionamiento prácticamente standar, hay un par de configuraciones pero nada de otro mundo, el principal interés está en el Dockerfile de php-fpm.
Lo que hace es tomar una base de PHP, agregarle las dependencias de composer y gd (para el manejo de gráficos) más git y ssh para clonar un repo e instalar los packetes.
Hace además un configuración básica de permisos.
La descarga la hace mediante el uso de claves privadas y públicas que se deben generar y guardar en la carpeta php-fpm, esas claves tiene que estar habilitadas en Bitbucket o Github.
Para generarlas:

```bash
ssh-keygen -q -t rsa -N '' -f repo-key
```

En el build (docker-compose build) descarga y empaqueta el codigo dentro de la imágen.
